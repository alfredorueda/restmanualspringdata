package com.example.library.Repositories;

import com.example.library.Model.Book;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface BookRepository extends PagingAndSortingRepository<Book, Long> {
	List<Book> findByTitleContains(@Param("title") String title);
	List<Book> findByNumSellsBetween(@Param("start") int start, @Param("end") int end);
	List<Book> findByNumSellsLessThan (@Param("less") int less);
	List<Book> findByNumSellsGreaterThan (@Param("greater") int greater);
	List<Book> findByAuthorId (@Param("id") Long id);
}
