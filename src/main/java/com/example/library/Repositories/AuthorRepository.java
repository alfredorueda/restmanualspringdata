package com.example.library.Repositories;

import com.example.library.Model.Author;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface AuthorRepository extends PagingAndSortingRepository<Author, Long> {
	List<Author> findByFirstNameContains(@Param("firstname") String firstName);
	List<Author> findByLastNameContains(@Param("lastname") String lastName);
	List<Author> findByBirthDateBetween(@Param("start") Date start, @Param("end") Date end);
}
