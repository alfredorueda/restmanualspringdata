package com.example.library.Controllers;

import com.example.library.Model.Author;
import com.example.library.Repositories.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;


// http://localhost:8080/authors
// http://localhost:8080/authors?begin=1428666843000&end=1428753258000
// http://localhost:8080/authors?begin=1428666843000&end=1428753247000
// http://localhost:8080/authors?firstname=j
// http://localhost:8080/authors?firstname=jane

//UPDATE
//{"firstName":"John","lastName":"Doe","birthDate":1428666843000}
//{"firstName":"Jane","lastName":"Doe","birthDate":1428753258000}


@RestController
@RequestMapping("/authors")
public class AuthorController {

	@Autowired
	private AuthorRepository authorRepository;

	@RequestMapping(method = POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Author save(@RequestBody Author author) {
		return authorRepository.save(author);
	}

	@RequestMapping(value = "/{id}", method = PUT)
	public Author update(@PathVariable Long id, @RequestBody Author author) {
		author.setId(id);
		authorRepository.save(author);
		return author;
	}

	@RequestMapping(value = "/{id}", method = DELETE)
	public void delete(@PathVariable Long id) {
		authorRepository.delete(id);
	}

	@RequestMapping(value = "/{id}", method = GET)
	public Author findById(@PathVariable Long id) {
		return authorRepository.findOne(id);
	}

	@RequestMapping( method = GET)
	public List<Author> findAll() {
		List<Author> authors = new ArrayList<Author>();
		Iterator<Author> iterator = authorRepository.findAll().iterator();

		while(iterator.hasNext()){
			authors.add(iterator.next());
		}

		return authors;
	}

	@RequestMapping(method = GET, params = "firstname")
	public List<Author> findByFirstName(String firstname){
		return authorRepository.findByFirstNameContains(firstname);
	}

	@RequestMapping(method = GET, params = "lastname")
	public List<Author> findByLastName(String lastname){
		return authorRepository.findByFirstNameContains(lastname);
	}

	@RequestMapping(method = GET, params = {"begin", "end"})
	public List<Author> findByBirthDateBetween(Long begin, Long end){
		return authorRepository.findByBirthDateBetween(new Date(begin), new Date(end));
	}




}
