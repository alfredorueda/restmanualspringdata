package com.example.library.Controllers;

import com.example.library.Model.Author;
import com.example.library.Model.Book;
import com.example.library.Repositories.AuthorRepository;
import com.example.library.Repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

// http://localhost:8080/authors/1/books
//  {"isbn":"1234567890","title":"example","numSells":1000,"publishedDate":1428667565000}
// {"isbn":"1234567892","title":"example2","numSells":2000,"publishedDate":1428667565000}

// http://localhost:8080/authors/2/books
// {"isbn":"1234567893","title":"example3","numSells":2000,"publishedDate":1428667565000}

// http://localhost:8080/books
// http://localhost:8080/book?title=ex
// http://localhost:8080/book?title=example3

@RestController
public class BookController {

	@Autowired
	private BookRepository bookRepository;

	@Autowired
	private AuthorRepository authorRepository;

	@RequestMapping(value="/authors/{authorId}/books", method = POST)
	public Book save(@PathVariable Long authorId, @RequestBody Book book) {

		Author author = authorRepository.findOne(authorId);

		book.setAuthor(author);

		return bookRepository.save(book);
	}

	@RequestMapping(value="/books", method = GET)
	public List<Book> findAll() {
		List<Book> books = new ArrayList<Book>();
		Iterator<Book> iterator = bookRepository.findAll().iterator();

		while(iterator.hasNext()){
			books.add(iterator.next());
		}

		return books;
	}

	@RequestMapping(value="/authors/{authorId}/books", method = GET)
	public List<Book> findAll(@PathVariable Long authorId){
		return bookRepository.findByAuthorId(authorId);
	}

	@RequestMapping(method = GET, params = "title")
	public List<Book> findByTitle(String title){
		return bookRepository.findByTitleContains(title);
	}


}
