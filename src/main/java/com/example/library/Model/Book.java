package com.example.library.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "books")
public class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@NotNull
	@Column(name = "isbn")
	@Size(min=10, max=13)
	private String isbn;

	@NotNull
	@ManyToOne
	@JsonIgnore
	private Author author;

	@NotNull
	@Column(name = "title")
	private String title;


	@Column(name = "numSells")
	@Min(0)
	private int numSells;


	@Column(name = "publishedDate")
	private Date publishedDate;

	public Book(){}

	public Book(Date publishedDate, int numSells, String title, String isbn) {
		this.publishedDate = publishedDate;
		this.numSells = numSells;
		this.title = title;
		this.isbn = isbn;
	}

	public Book(String isbn, Author author, String title, int numSells, Date publishedDate) {
		this.isbn = isbn;
		this.author = author;
		this.title = title;
		this.numSells = numSells;
		this.publishedDate = publishedDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getNumSells() {
		return numSells;
	}

	public void setNumSells(int numSells) {
		this.numSells = numSells;
	}

	public Date getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}

	@Override
	public String toString() {
		return "Book{" +
				"id=" + id +
				", isbn='" + isbn + '\'' +
				", author=" + author +
				", title='" + title + '\'' +
				", numSells=" + numSells +
				", publishedDate=" + publishedDate +
				'}';
	}
}